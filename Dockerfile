FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install curl apache2 php7.0 php7.0-dev libapache2-mod-php7.0 php7.0-mcrypt php-pear libcurl4-openssl-dev libsasl2-dev libcurl4-openssl-dev pkg-config git -y

RUN apt-get install php7.0-curl php7.0-gd php7.0-mysql php7.0-mbstring php7.0-bcmath php7.0-curl php-memcached php-intl -y

RUN pecl install redis

RUN printf "opcache.enable=1\n" >> /etc/php/7.0/apache2/php.ini

RUN printf "opcache.memory_consumption=128\n" >> /etc/php/7.0/apache2/php.ini

RUN printf "opcache_revalidate_freq=3600 \n" >> /etc/php/7.0/apache2/php.ini

RUN printf "opcache.max_accelerated_files=4000 \n" >> /etc/php/7.0/apache2/php.ini

RUN printf "extension=redis.so\n" >> /etc/php/7.0/apache2/php.ini

RUN /bin/bash -c 'curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | bash'

RUN apt-get install php7.0-phalcon

RUN phpenmod opcache

COPY 000-default.conf /etc/apache2/sites-available/

RUN a2enmod rewrite && a2ensite 000-default

RUN a2enmod headers

COPY apache2foreground /usr/local/bin/

RUN chmod +X /usr/local/bin/apache2foreground

WORKDIR /var/www/

EXPOSE 80

CMD ["sh","/usr/local/bin/apache2foreground"]
